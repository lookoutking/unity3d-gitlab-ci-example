@echo off
setlocal

echo Building for %BUILD_TARGET%

set BUILD_PATH=.\Builds\%BUILD_TARGET%\
if not exist "%BUILD_PATH%" mkdir "%BUILD_PATH%"

if exist "%UNITY_EXECUTABLE%" (
    echo unity exists.
) else (
    echo unity does not exist.
)
endlocal

"%UNITY_EXECUTABLE%" -projectPath .\ -quit -batchmode -nographics -buildTarget %BUILD_TARGET% -customBuildTarget %BUILD_TARGET% -customBuildName %BUILD_NAME% -customBuildPath %BUILD_PATH% -executeMethod BuildCommand.PerformBuild

set UNITY_EXIT_CODE=%errorlevel%

if %UNITY_EXIT_CODE% equ 0 (
  echo Run succeeded, no failures occurred
) else if %UNITY_EXIT_CODE% equ 2 (
  echo Run succeeded, some tests failed
) else if %UNITY_EXIT_CODE% equ 3 (
  echo Run failure (other failure)
) else (
  echo Unexpected exit code %UNITY_EXIT_CODE%
)

dir /b /a-d "%BUILD_PATH%"
if not exist "%BUILD_PATH%*" exit /b 1

endlocal
