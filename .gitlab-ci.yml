stages:
  - prepare
  - build_and_test
  - deploy

# If you are looking for a place where to add 'UNITY_LICENSE_FILE' and other secrets, please visit your project's gitlab page:
# settings > CI/CD > Variables instead
variables:
  BUILD_NAME: ExampleProjectName
  UNITY_ACTIVATION_FILE: ./unity3d.alf
  IMAGE: unityci/editor # https://hub.docker.com/r/unityci/editor
  IMAGE_VERSION: 2 # This will automatically use latest v1.x.x, see https://github.com/game-ci/docker/releases
  UNITY_DIR: $CI_PROJECT_DIR # this needs to be an absolute path. Defaults to the root of your tree.
  # You can expose this in Unity via Application.version
  UNITY_EXECUTABLE: C:\Program Files\Unity\Hub\Editor\$UNITY_VERSION\Editor\Unity.exe
  VERSION_NUMBER_VAR: $CI_COMMIT_REF_SLUG-$CI_PIPELINE_ID-$CI_JOB_ID
  VERSION_BUILD_VAR: $CI_PIPELINE_IID

image: $IMAGE:$UNITY_VERSION-base-$IMAGE_VERSION

get-unity-version:
  image: alpine
  stage: prepare
  variables:
    GIT_DEPTH: 1
  script:
    - echo UNITY_VERSION=$(cat $UNITY_DIR/ProjectSettings/ProjectVersion.txt | grep "m_EditorVersion:.*" | awk '{ print $2}') | tee prepare.env
  artifacts:
    reports:
      dotenv: prepare.env

.unity_before_script: &unity_before_script
  before_script:
    - chmod +x ./ci/before_script.sh && ./ci/before_script.sh
  needs:
    - job: get-unity-version
      artifacts: true

.cache: &cache
  cache:
    key: "$CI_PROJECT_NAMESPACE-$CI_PROJECT_NAME-$CI_COMMIT_REF_SLUG-$TEST_PLATFORM"
    paths:
      - $UNITY_DIR/Library/

.license: &license
  rules:
    - if: "$UNITY_LICENSE != null"
      when: always

.unity_defaults: &unity_defaults
  <<:
    # - *unity_before_script
    - *cache
    - *license

# run this job when you need to request a license
# you may need to follow activation steps from documentation
get-activation-file:
  <<: *unity_before_script
  rules:
    - if: "$UNITY_LICENSE == null"
      when: manual
  stage: prepare
  script:
    - chmod +x ./ci/get_activation_file.sh && ./ci/get_activation_file.sh
  artifacts:
    paths:
      - $UNITY_ACTIVATION_FILE
    expire_in: 10 min # Expiring this as artifacts may contain sensitive data and should not be kept public

.test: &test
  stage: build_and_test
  <<: *unity_defaults
  script:
    - chmod +x ./ci/test.sh && ./ci/test.sh
  artifacts:
    when: always
    expire_in: 2 weeks
  # https://gitlab.com/gableroux/unity3d-gitlab-ci-example/-/issues/83
  # you may need to remove or replace these to fit your need if you are using your own runners
  tags:
    - gitlab-org
  coverage: /<Linecoverage>(.*?)</Linecoverage>/

# Tests without junit reporting results in GitLab
# test-playmode:
#   <<: *test
#   variables:
#     TEST_PLATFORM: playmode
#     TESTING_TYPE: NUNIT

# test-editmode:
#   <<: *test
#   variables:
#     TEST_PLATFORM: editmode
#     TESTING_TYPE: NUNIT

# uncomment the following blocks if you'd like to have junit reporting unity test results in gitlab
# We currently have the following issue which prevents it from working right now, but you can give
# a hand if you're interested in this feature:
# https://gitlab.com/gableroux/unity3d-gitlab-ci-example/-/issues/151

.build: &build
  stage: build_and_test
  <<: *unity_defaults
  script:
    - cmd /c "ci\build.bat"
  artifacts:
    paths:
      - $UNITY_DIR/Builds/
  # https://gitlab.com/gableroux/unity3d-gitlab-ci-example/-/issues/83
  # you may need to remove or replace these to fit your need if you are using your own runners
  tags:
    - unity-windows

build-test:
  <<: *build
  image: $IMAGE:windows-$UNITY_VERSION-base-2
  variables:
    BUILD_TARGET: StandaloneWindows64

pages:
  image: alpine:latest
  stage: deploy
  script:
    - mv "$UNITY_DIR/Builds/WebGL/${BUILD_NAME}" public
  artifacts:
    paths:
      - public
  only:
    - $CI_DEFAULT_BRANCH

workflow:
  rules:
    - if: $CI_MERGE_REQUEST_ID
      when: never
    - if: $CI_COMMIT_TAG
      when: never
    - when: always
